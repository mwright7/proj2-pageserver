from flask import Flask, render_template, request
import os

app = Flask(__name__)
'''
@app.route('/')
def index():
    return render_template("trivia.html"), 200
'''

@app.route("/<path:gohere>")
def display_page(gohere):
    print(gohere) 
    forbidden = ["..", "//", "~"]
    for item in forbidden:
        if item in gohere:
            return render_template("403.html"), 403
    if os.path.isfile("templates/" + gohere):
        return render_template(str(gohere)), 200
    else:
        return render_template("404.html"), 404

#errors that arent caught above
@app.errorhandler(404)
def page_not_found(error):
    print('handled')
    return render_template("404.html"),404

@app.errorhandler(403)
def access_denied(error):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
